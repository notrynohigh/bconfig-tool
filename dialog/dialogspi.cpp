#include "dialogspi.h"

#include "ui_dialogspi.h"

Dialogspi::Dialogspi(QWidget *parent) : QDialog(parent), ui(new Ui::Dialogspi)
{
    QStringList gpio = {"B_HAL_GPIOA", "B_HAL_GPIOB", "B_HAL_GPIOC", "B_HAL_GPIOD",
                        "B_HAL_GPIOE", "B_HAL_GPIOF", "B_HAL_GPIOG"};
    QStringList pin  = {"B_HAL_PIN0",  "B_HAL_PIN1",  "B_HAL_PIN2",  "B_HAL_PIN3",
                       "B_HAL_PIN4",  "B_HAL_PIN5",  "B_HAL_PIN6",  "B_HAL_PIN7",
                       "B_HAL_PIN8",  "B_HAL_PIN9",  "B_HAL_PIN10", "B_HAL_PIN11",
                       "B_HAL_PIN12", "B_HAL_PIN13", "B_HAL_PIN14", "B_HAL_PIN15"};
    ui->setupUi(this);

    ui->checkbox_spi_sim->setChecked(false);

    ui->combox_spi->setEnabled(true);
    ui->combox_clk_pin->setEnabled(false);
    ui->combox_mosi_pin->setEnabled(false);
    ui->combox_miso_pin->setEnabled(false);

    ui->combox_clk_port->setEnabled(false);
    ui->combox_mosi_port->setEnabled(false);
    ui->combox_miso_port->setEnabled(false);

    ui->combox_spi_cpha->setEnabled(false);
    ui->combox_spi_cpol->setEnabled(false);

    ui->combox_spi->addItems({"B_HAL_SPI_1", "B_HAL_SPI_2", "B_HAL_SPI_3", "B_HAL_SPI_4"});

    ui->combox_cs_port->addItems(gpio);
    ui->combox_clk_port->addItems(gpio);
    ui->combox_mosi_port->addItems(gpio);
    ui->combox_miso_port->addItems(gpio);

    ui->combox_cs_pin->addItems(pin);
    ui->combox_clk_pin->addItems(pin);
    ui->combox_mosi_pin->addItems(pin);
    ui->combox_miso_pin->addItems(pin);

    ui->combox_spi_cpha->addItem("0");
    ui->combox_spi_cpha->addItem("1");

    ui->combox_spi_cpol->addItem("0");
    ui->combox_spi_cpol->addItem("1");

    spi_cfg_str.clear();

    this->setModal(true);
}

Dialogspi::~Dialogspi()
{
    delete ui;
}

void Dialogspi::on_checkbox_spi_sim_stateChanged(int arg1)
{
    if (ui->checkbox_spi_sim->isChecked())
    {
        ui->combox_spi->setEnabled(false);
        ui->combox_clk_pin->setEnabled(true);
        ui->combox_mosi_pin->setEnabled(true);
        ui->combox_miso_pin->setEnabled(true);

        ui->combox_clk_port->setEnabled(true);
        ui->combox_mosi_port->setEnabled(true);
        ui->combox_miso_port->setEnabled(true);

        ui->combox_spi_cpha->setEnabled(true);
        ui->combox_spi_cpol->setEnabled(true);
    }
    else
    {
        ui->combox_spi->setEnabled(true);
        ui->combox_clk_pin->setEnabled(false);
        ui->combox_mosi_pin->setEnabled(false);
        ui->combox_miso_pin->setEnabled(false);

        ui->combox_clk_port->setEnabled(false);
        ui->combox_mosi_port->setEnabled(false);
        ui->combox_miso_port->setEnabled(false);

        ui->combox_spi_cpha->setEnabled(false);
        ui->combox_spi_cpol->setEnabled(false);
    }
}

void Dialogspi::on_buttonbox_accepted()
{
    QString cs = ".cs = {" + ui->combox_cs_port->currentText() + ", " +
                 ui->combox_cs_pin->currentText() + "}, ";
    QString is_simulation = ".is_simulation = ";
    QString _if           = "";
    if (ui->checkbox_spi_sim->isChecked())
    {
        is_simulation.append("1, ");

        _if.append("._if.simulating_spi.clk = {" + ui->combox_clk_port->currentText() + ", " +
                   ui->combox_clk_pin->currentText() + "}, ");
        _if.append("._if.simulating_spi.mosi = {" + ui->combox_mosi_port->currentText() + ", " +
                   ui->combox_mosi_pin->currentText() + "}, ");
        _if.append("._if.simulating_spi.miso = {" + ui->combox_miso_port->currentText() + ", " +
                   ui->combox_miso_pin->currentText() + "}, ");
        _if.append("._if.simulating_spi.CPOL = " + ui->combox_spi_cpol->currentText() + ", ");
        _if.append("._if.simulating_spi.CPHA = " + ui->combox_spi_cpha->currentText() + ", ");
    }
    else
    {
        is_simulation.append("0, ");
        _if.append("._if.spi = " + ui->combox_spi->currentText() + ", ");
    }

    spi_cfg_str = "{" + is_simulation + " " + cs + " " + _if + "}";
    emit send_string(spi_cfg_str);
}
