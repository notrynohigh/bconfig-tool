#ifndef DIALOGUART_H
#define DIALOGUART_H

#include <QDialog>

namespace Ui
{
class Dialoguart;
}

class Dialoguart : public QDialog
{
    Q_OBJECT

public:
    explicit Dialoguart(QWidget *parent = nullptr);
    ~Dialoguart();

    QString uart_cfg_str;

signals:
    void send_string(QString);

private slots:
    void on_buttonbox_accepted();

private:
    Ui::Dialoguart *ui;
};

#endif  // DIALOGUART_H
