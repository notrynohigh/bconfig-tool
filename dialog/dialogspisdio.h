#ifndef DIALOGSPISDIO_H
#define DIALOGSPISDIO_H

#include <QDialog>

namespace Ui
{
class Dialogspisdio;
}

class Dialogspisdio : public QDialog
{
    Q_OBJECT

public:
    explicit Dialogspisdio(QWidget *parent = nullptr);
    ~Dialogspisdio();

    QString spisdio_cfg_str;

signals:
    void send_string(QString);

private slots:
    void on_buttonbox_accepted();
    void rec_spisdio_string(QString str);
    void on_button_config_clicked();

private:
    Ui::Dialogspisdio *ui;
};

#endif  // DIALOGSPII2C_H
