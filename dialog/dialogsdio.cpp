#include "dialogsdio.h"

#include "ui_dialogsdio.h"

Dialogsdio::Dialogsdio(QWidget *parent) : QDialog(parent), ui(new Ui::Dialogsdio)
{
    QStringList uart = {"B_HAL_SDIO_1", "B_HAL_SDIO_2"};
    ui->setupUi(this);
    ui->combox_sdio->addItems(uart);

    sdio_cfg_str.clear();

    this->setModal(true);
}

Dialogsdio::~Dialogsdio()
{
    delete ui;
}

void Dialogsdio::on_buttonbox_accepted()
{
    sdio_cfg_str = "{" + ui->combox_sdio->currentText() + "}";
    emit send_string(sdio_cfg_str);
}
