#include "dialogspii2c.h"

#include "ui_dialogspii2c.h"
#include "dialogi2c.h"
#include "dialogspi.h"

#include "QDebug"

Dialogspii2c::Dialogspii2c(QWidget *parent) : QDialog(parent), ui(new Ui::Dialogspii2c)
{
    ui->setupUi(this);

    spii2c_cfg_str.clear();

    this->setModal(true);
}

Dialogspii2c::~Dialogspii2c()
{
    delete ui;
}

void Dialogspii2c::on_buttonbox_accepted()
{
    emit send_string(spii2c_cfg_str);
}

void Dialogspii2c::on_button_config_clicked()
{
    if (ui->radio_i2c->isChecked())
    {
        DialogI2C *_ifi2c = new DialogI2C(this);
        connect(_ifi2c, SIGNAL(send_string(QString)), this, SLOT(rec_spii2c_string(QString)));
        _ifi2c->show();
    }
    else if (ui->radio_spi->isChecked())
    {
        Dialogspi *_ifspi = new Dialogspi(this);
        connect(_ifspi, SIGNAL(send_string(QString)), this, SLOT(rec_spii2c_string(QString)));
        _ifspi->show();
    }
}

void Dialogspii2c::rec_spii2c_string(QString str)
{
    spii2c_cfg_str.clear();
    if(ui->radio_spi->isChecked())
    {
        spii2c_cfg_str.append("{.is_spi = 1, ._if._spi = " + str + "}");
    }
    else
    {
        spii2c_cfg_str.append("{.is_spi = 0, ._if._i2c = " + str + "}");
    }
}
