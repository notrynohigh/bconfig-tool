#ifndef DIALOGSPI_H
#define DIALOGSPI_H

#include <QDialog>

namespace Ui
{
class Dialogspi;
}

class Dialogspi : public QDialog
{
    Q_OBJECT

public:
    explicit Dialogspi(QWidget *parent = nullptr);
    ~Dialogspi();

    QString spi_cfg_str;

signals:
    void send_string(QString);

private slots:
    void on_checkbox_spi_sim_stateChanged(int arg1);

    void on_buttonbox_accepted();

private:
    Ui::Dialogspi *ui;
};

#endif  // DIALOGSPI_H
