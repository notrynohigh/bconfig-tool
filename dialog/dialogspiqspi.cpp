#include "dialogspiqspi.h"

#include "ui_dialogspiqspi.h"
#include "dialogqspi.h"
#include "dialogspi.h"

#include "QDebug"

Dialogspiqspi::Dialogspiqspi(QWidget *parent) : QDialog(parent), ui(new Ui::Dialogspiqspi)
{
    ui->setupUi(this);

    spiqspi_cfg_str.clear();

    this->setModal(true);
}

Dialogspiqspi::~Dialogspiqspi()
{
    delete ui;
}

void Dialogspiqspi::on_buttonbox_accepted()
{
    emit send_string(spiqspi_cfg_str);
}

void Dialogspiqspi::on_button_config_clicked()
{
    if (ui->radio_qspi->isChecked())
    {
        Dialogqspi *_ifqspi = new Dialogqspi(this);
        connect(_ifqspi, SIGNAL(send_string(QString)), this, SLOT(rec_spiqspi_string(QString)));
        _ifqspi->show();
    }
    else if (ui->radio_spi->isChecked())
    {
        Dialogspi *_ifspi = new Dialogspi(this);
        connect(_ifspi, SIGNAL(send_string(QString)), this, SLOT(rec_spiqspi_string(QString)));
        _ifspi->show();
    }
}

void Dialogspiqspi::rec_spiqspi_string(QString str)
{
    spiqspi_cfg_str.clear();
    if(ui->radio_spi->isChecked())
    {
        spiqspi_cfg_str.append("{.is_spi = 1, ._if._spi = " + str + "}");
    }
    else
    {
        spiqspi_cfg_str.append("{.is_spi = 0, ._if._qspi = " + str + "}");
    }
}
