#ifndef DIALOGLCD_H
#define DIALOGLCD_H

#include <QDialog>

namespace Ui
{
class Dialoglcd;
}

class Dialoglcd : public QDialog
{
    Q_OBJECT

public:
    explicit Dialoglcd(QWidget *parent = nullptr);
    ~Dialoglcd();
    void    all_stat_changed(int arg1);
    QString lcd_cfg_str;

signals:
    void send_string(QString);

private slots:
    void on_buttonbox_accepted();
    void on_checkbox_spi_sim_stateChanged(int arg1);

    void on_radioButtonAddr_clicked();

    void on_radioButtonSpi_clicked();

    void on_radioButtonOther_clicked();

private:
    Ui::Dialoglcd *ui;
};

#endif  // DIALOGLCD_H
