#include "dialoglcd.h"

#include "ui_dialoglcd.h"

Dialoglcd::Dialoglcd(QWidget *parent) : QDialog(parent), ui(new Ui::Dialoglcd)
{
    QStringList gpio = {"B_HAL_GPIOA", "B_HAL_GPIOB", "B_HAL_GPIOC", "B_HAL_GPIOD",
                        "B_HAL_GPIOE", "B_HAL_GPIOF", "B_HAL_GPIOG"};
    QStringList pin  = {"B_HAL_PIN0",  "B_HAL_PIN1",  "B_HAL_PIN2",  "B_HAL_PIN3",  "B_HAL_PIN4",
                       "B_HAL_PIN5",  "B_HAL_PIN6",  "B_HAL_PIN7",  "B_HAL_PIN8",  "B_HAL_PIN9",
                       "B_HAL_PIN10", "B_HAL_PIN11", "B_HAL_PIN12", "B_HAL_PIN13", "B_HAL_PIN14",
                       "B_HAL_PIN15", "B_HAL_PINAll"};
    ui->setupUi(this);

    ui->combox_data_pin->setEnabled(true);
    ui->combox_rs_pin->setEnabled(true);
    ui->combox_rd_pin->setEnabled(true);
    ui->combox_wr_pin->setEnabled(true);
    ui->combox_cs_pin->setEnabled(true);

    ui->combox_data_port->setEnabled(true);
    ui->combox_rs_port->setEnabled(true);
    ui->combox_rd_port->setEnabled(true);
    ui->combox_wr_port->setEnabled(true);
    ui->combox_cs_port->setEnabled(true);

    ui->line_edit_addr->setEnabled(false);
    ui->line_edit_addr->setText("0x00");

    ui->checkbox_spi_sim->setEnabled(false);
    ui->checkbox_spi_sim->setChecked(false);
    ui->combox_spi->setEnabled(false);
    ui->combox_clk_pin->setEnabled(false);
    ui->combox_mosi_pin->setEnabled(false);
    ui->combox_miso_pin->setEnabled(false);
    ui->combox_clk_port->setEnabled(false);
    ui->combox_mosi_port->setEnabled(false);
    ui->combox_miso_port->setEnabled(false);
    ui->combox_spi_cpha->setEnabled(false);
    ui->combox_spi_cpol->setEnabled(false);
    ui->combox_cs_pin_2->setEnabled(false);
    ui->combox_cs_port_2->setEnabled(false);
    ui->combox_rs_pin_2->setEnabled(false);
    ui->combox_rs_port_2->setEnabled(false);

    ui->combox_spi->addItems({"B_HAL_SPI_1", "B_HAL_SPI_2", "B_HAL_SPI_3", "B_HAL_SPI_4"});

    ui->combox_cs_port_2->addItems(gpio);
    ui->combox_clk_port->addItems(gpio);
    ui->combox_mosi_port->addItems(gpio);
    ui->combox_miso_port->addItems(gpio);
    ui->combox_rs_port_2->addItems(gpio);

    ui->combox_cs_pin_2->addItems(pin);
    ui->combox_clk_pin->addItems(pin);
    ui->combox_mosi_pin->addItems(pin);
    ui->combox_miso_pin->addItems(pin);
    ui->combox_rs_pin_2->addItems(pin);
    ui->combox_spi_cpha->addItem("0");
    ui->combox_spi_cpha->addItem("1");
    ui->combox_spi_cpol->addItem("0");
    ui->combox_spi_cpol->addItem("1");

    ui->combox_data_port->addItems(gpio);
    ui->combox_rs_port->addItems(gpio);
    ui->combox_rd_port->addItems(gpio);
    ui->combox_wr_port->addItems(gpio);
    ui->combox_cs_port->addItems(gpio);

    ui->combox_data_pin->addItems(pin);
    ui->combox_rs_pin->addItems(pin);
    ui->combox_rd_pin->addItems(pin);
    ui->combox_wr_pin->addItems(pin);
    ui->combox_cs_pin->addItems(pin);

    lcd_cfg_str.clear();
    this->setModal(true);
}

Dialoglcd::~Dialoglcd()
{
    delete ui;
}

void Dialoglcd::on_buttonbox_accepted()
{
    QString if_type = ".if_type = ";
    QString _if     = "";

    QString rs = "._if._spi.rs = {" + ui->combox_rs_port_2->currentText() + ", " +
                 ui->combox_rs_pin_2->currentText() + "}, ";
    QString cs = ".cs = {" + ui->combox_cs_port_2->currentText() + ", " +
                 ui->combox_cs_pin_2->currentText() + "}, ";
    QString is_simulation = ".is_simulation = ";
    QString _spi_if       = "";

    if (ui->radioButtonAddr->isChecked())
    {
        if_type.append("1, ");
        _if.append("._if.rw_addr = " + ui->line_edit_addr->text() + ", ");
    }
    else if (ui->radioButtonOther->isChecked())
    {
        if_type.append("0, ");
        _if.append("._if._io.data = {" + ui->combox_data_port->currentText() + ", " +
                   ui->combox_data_pin->currentText() + "}, ");
        _if.append("._if._io.rs = {" + ui->combox_rs_port->currentText() + ", " +
                   ui->combox_rs_pin->currentText() + "}, ");
        _if.append("._if._io.rd = {" + ui->combox_rd_port->currentText() + ", " +
                   ui->combox_rd_pin->currentText() + "}, ");
        _if.append("._if._io.wr = {" + ui->combox_wr_port->currentText() + ", " +
                   ui->combox_wr_pin->currentText() + "}, ");
        _if.append("._if._io.cs = {" + ui->combox_cs_port->currentText() + ", " +
                   ui->combox_cs_pin->currentText() + "}, ");
    }
    else
    {
        if_type.append("2, ");
        if (ui->checkbox_spi_sim->isChecked())
        {
            is_simulation.append("1, ");

            _spi_if.append("._if.simulating_spi.clk = {" + ui->combox_clk_port->currentText() +
                           ", " + ui->combox_clk_pin->currentText() + "}, ");
            _spi_if.append("._if.simulating_spi.mosi = {" + ui->combox_mosi_port->currentText() +
                           ", " + ui->combox_mosi_pin->currentText() + "}, ");
            _spi_if.append("._if.simulating_spi.miso = {" + ui->combox_miso_port->currentText() +
                           ", " + ui->combox_miso_pin->currentText() + "}, ");
            _spi_if.append("._if.simulating_spi.CPOL = " + ui->combox_spi_cpol->currentText() +
                           ", ");
            _spi_if.append("._if.simulating_spi.CPHA = " + ui->combox_spi_cpha->currentText() +
                           ", ");
        }
        else
        {
            is_simulation.append("0, ");
            _spi_if.append("._if.spi = " + ui->combox_spi->currentText() + ", ");
        }
        _if.append("._if._spi._spi = {" + is_simulation + " " + cs + " " + _spi_if + "},");
        _if.append(rs);
    }

    lcd_cfg_str = "{" + if_type + " " + _if + "}";
    emit send_string(lcd_cfg_str);
}

void Dialoglcd::all_stat_changed(int arg1)
{
    ui->checkbox_spi_sim->setEnabled(false);
    ui->checkbox_spi_sim->setChecked(false);
    ui->combox_spi->setEnabled(false);
    ui->combox_clk_pin->setEnabled(false);
    ui->combox_mosi_pin->setEnabled(false);
    ui->combox_miso_pin->setEnabled(false);
    ui->combox_clk_port->setEnabled(false);
    ui->combox_mosi_port->setEnabled(false);
    ui->combox_miso_port->setEnabled(false);
    ui->combox_spi_cpha->setEnabled(false);
    ui->combox_spi_cpol->setEnabled(false);
    ui->combox_cs_pin_2->setEnabled(false);
    ui->combox_cs_port_2->setEnabled(false);
    ui->combox_rs_pin_2->setEnabled(false);
    ui->combox_rs_port_2->setEnabled(false);
    ui->combox_data_pin->setEnabled(false);
    ui->combox_rs_pin->setEnabled(false);
    ui->combox_rd_pin->setEnabled(false);
    ui->combox_wr_pin->setEnabled(false);
    ui->combox_cs_pin->setEnabled(false);

    ui->combox_data_port->setEnabled(false);
    ui->combox_rs_port->setEnabled(false);
    ui->combox_rd_port->setEnabled(false);
    ui->combox_wr_port->setEnabled(false);
    ui->combox_cs_port->setEnabled(false);
    ui->line_edit_addr->setEnabled(false);

    if (arg1 == 1)  // rw_addr
    {
        ui->line_edit_addr->setEnabled(true);
    }
    else if (arg1 == 0)  // other
    {
        ui->combox_data_pin->setEnabled(true);
        ui->combox_rs_pin->setEnabled(true);
        ui->combox_rd_pin->setEnabled(true);
        ui->combox_wr_pin->setEnabled(true);
        ui->combox_cs_pin->setEnabled(true);

        ui->combox_data_port->setEnabled(true);
        ui->combox_rs_port->setEnabled(true);
        ui->combox_rd_port->setEnabled(true);
        ui->combox_wr_port->setEnabled(true);
        ui->combox_cs_port->setEnabled(true);
    }
    else
    {
        ui->checkbox_spi_sim->setEnabled(true);
        ui->checkbox_spi_sim->setChecked(false);
        ui->combox_spi->setEnabled(true);
        ui->combox_cs_pin_2->setEnabled(true);
        ui->combox_cs_port_2->setEnabled(true);
        ui->combox_rs_pin_2->setEnabled(true);
        ui->combox_rs_port_2->setEnabled(true);
    }
}

void Dialoglcd::on_checkbox_spi_sim_stateChanged(int arg1)
{
    if (ui->checkbox_spi_sim->isChecked())
    {
        ui->combox_spi->setEnabled(false);
        ui->combox_clk_pin->setEnabled(true);
        ui->combox_mosi_pin->setEnabled(true);
        ui->combox_miso_pin->setEnabled(true);

        ui->combox_clk_port->setEnabled(true);
        ui->combox_mosi_port->setEnabled(true);
        ui->combox_miso_port->setEnabled(true);

        ui->combox_spi_cpha->setEnabled(true);
        ui->combox_spi_cpol->setEnabled(true);
    }
    else
    {
        ui->combox_spi->setEnabled(true);
        ui->combox_clk_pin->setEnabled(false);
        ui->combox_mosi_pin->setEnabled(false);
        ui->combox_miso_pin->setEnabled(false);

        ui->combox_clk_port->setEnabled(false);
        ui->combox_mosi_port->setEnabled(false);
        ui->combox_miso_port->setEnabled(false);

        ui->combox_spi_cpha->setEnabled(false);
        ui->combox_spi_cpol->setEnabled(false);
    }
}

void Dialoglcd::on_radioButtonAddr_clicked()
{
    all_stat_changed(1);
}

void Dialoglcd::on_radioButtonSpi_clicked()
{
    all_stat_changed(2);
}

void Dialoglcd::on_radioButtonOther_clicked()
{
    all_stat_changed(0);
}
