#ifndef DIALOGGPIO_H
#define DIALOGGPIO_H

#include <QDialog>

namespace Ui
{
class Dialoggpio;
}

class Dialoggpio : public QDialog
{
    Q_OBJECT

public:
    explicit Dialoggpio(QWidget *parent = nullptr);
    ~Dialoggpio();

    QString gpio_cfg_str;

signals:
    void send_string(QString);

private slots:
    void on_buttonbox_accepted();

private:
    Ui::Dialoggpio *ui;
};

#endif  // DIALOGGPIO_H
