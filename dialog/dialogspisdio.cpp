#include "dialogspisdio.h"

#include "ui_dialogspisdio.h"
#include "dialogsdio.h"
#include "dialogspi.h"

#include "QDebug"

Dialogspisdio::Dialogspisdio(QWidget *parent) : QDialog(parent), ui(new Ui::Dialogspisdio)
{
    ui->setupUi(this);

    spisdio_cfg_str.clear();

    this->setModal(true);
}

Dialogspisdio::~Dialogspisdio()
{
    delete ui;
}

void Dialogspisdio::on_buttonbox_accepted()
{
    emit send_string(spisdio_cfg_str);
}

void Dialogspisdio::on_button_config_clicked()
{
    if (ui->radio_sdio->isChecked())
    {
        Dialogsdio *_ifsdio = new Dialogsdio(this);
        connect(_ifsdio, SIGNAL(send_string(QString)), this, SLOT(rec_spisdio_string(QString)));
        _ifsdio->show();
    }
    else if (ui->radio_spi->isChecked())
    {
        Dialogspi *_ifspi = new Dialogspi(this);
        connect(_ifspi, SIGNAL(send_string(QString)), this, SLOT(rec_spisdio_string(QString)));
        _ifspi->show();
    }
}

void Dialogspisdio::rec_spisdio_string(QString str)
{
    spisdio_cfg_str.clear();
    if(ui->radio_spi->isChecked())
    {
        spisdio_cfg_str.append("{.is_spi = 1, ._if._spi = " + str + "}");
    }
    else
    {
        spisdio_cfg_str.append("{.is_spi = 0, ._if._sdio = " + str + "}");
    }
}
