#include "dialoggpio.h"

#include "ui_dialoggpio.h"

Dialoggpio::Dialoggpio(QWidget *parent) : QDialog(parent), ui(new Ui::Dialoggpio)
{
    QStringList gpio_port = {"B_HAL_GPIOA", "B_HAL_GPIOB", "B_HAL_GPIOC", "B_HAL_GPIOD",
                             "B_HAL_GPIOE", "B_HAL_GPIOF", "B_HAL_GPIOG"};
    QStringList gpio_pin  = {"B_HAL_PIN0",  "B_HAL_PIN1",  "B_HAL_PIN2",  "B_HAL_PIN3",
                            "B_HAL_PIN4",  "B_HAL_PIN5",  "B_HAL_PIN6",  "B_HAL_PIN7",
                            "B_HAL_PIN8",  "B_HAL_PIN9",  "B_HAL_PIN10", "B_HAL_PIN11",
                            "B_HAL_PIN12", "B_HAL_PIN13", "B_HAL_PIN14", "B_HAL_PIN15"};
    ui->setupUi(this);
    ui->combox_gpio_port->addItems(gpio_port);
    ui->combox_gpio_pin->addItems(gpio_pin);

    gpio_cfg_str.clear();

    this->setModal(true);
}

Dialoggpio::~Dialoggpio()
{
    delete ui;
}

void Dialoggpio::on_buttonbox_accepted()
{
    gpio_cfg_str =
        "{" + ui->combox_gpio_port->currentText() + ", " + ui->combox_gpio_pin->currentText() + "}";
    emit send_string(gpio_cfg_str);
}
