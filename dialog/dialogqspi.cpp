#include "dialogqspi.h"

#include "ui_dialogqspi.h"

Dialogqspi::Dialogqspi(QWidget *parent) : QDialog(parent), ui(new Ui::Dialogqspi)
{
    QStringList qspi = {"B_HAL_QSPI_1", "B_HAL_QSPI_2"};
    ui->setupUi(this);
    ui->combox_qspi->addItems(qspi);

    qspi_cfg_str.clear();

    this->setModal(true);
}

Dialogqspi::~Dialogqspi()
{
    delete ui;
}

void Dialogqspi::on_buttonbox_accepted()
{
    qspi_cfg_str = "{" + ui->combox_qspi->currentText() + "}";
    emit send_string(qspi_cfg_str);
}
