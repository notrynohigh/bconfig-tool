#ifndef DIALOGSDIO_H
#define DIALOGSDIO_H

#include <QDialog>

namespace Ui
{
class Dialogsdio;
}

class Dialogsdio : public QDialog
{
    Q_OBJECT

public:
    explicit Dialogsdio(QWidget *parent = nullptr);
    ~Dialogsdio();

    QString sdio_cfg_str;

signals:
    void send_string(QString);

private slots:
    void on_buttonbox_accepted();

private:
    Ui::Dialogsdio *ui;
};

#endif  // DIALOGUART_H
