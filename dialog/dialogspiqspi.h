#ifndef DIALOGSPIQSPI_H
#define DIALOGSPIQSPI_H

#include <QDialog>

namespace Ui
{
class Dialogspiqspi;
}

class Dialogspiqspi : public QDialog
{
    Q_OBJECT

public:
    explicit Dialogspiqspi(QWidget *parent = nullptr);
    ~Dialogspiqspi();

    QString spiqspi_cfg_str;

signals:
    void send_string(QString);

private slots:
    void on_buttonbox_accepted();
    void rec_spiqspi_string(QString str);
    void on_button_config_clicked();

private:
    Ui::Dialogspiqspi *ui;
};

#endif  // DIALOGSPII2C_H
