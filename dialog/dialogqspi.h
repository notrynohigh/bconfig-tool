#ifndef DIALOGQSPI_H
#define DIALOGQSPI_H

#include <QDialog>

namespace Ui
{
class Dialogqspi;
}

class Dialogqspi : public QDialog
{
    Q_OBJECT

public:
    explicit Dialogqspi(QWidget *parent = nullptr);
    ~Dialogqspi();

    QString qspi_cfg_str;

signals:
    void send_string(QString);

private slots:
    void on_buttonbox_accepted();

private:
    Ui::Dialogqspi *ui;
};

#endif  // DIALOGUART_H
