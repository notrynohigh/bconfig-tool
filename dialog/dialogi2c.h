#ifndef DIALOGI2C_H
#define DIALOGI2C_H

#include <QDialog>

namespace Ui
{
class DialogI2C;
}

class DialogI2C : public QDialog
{
    Q_OBJECT

public:
    explicit DialogI2C(QWidget *parent = nullptr);
    ~DialogI2C();
    QString i2c_cfg_str;

signals:
    void send_string(QString);

private slots:
    void on_i2c_checkbox_sim_stateChanged(int arg1);

    void on_buttonbox_i2c_accepted();

private:
    Ui::DialogI2C *ui;
};

#endif  // DIALOGI2C_H
