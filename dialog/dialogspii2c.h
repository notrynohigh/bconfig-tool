#ifndef DIALOGSPII2C_H
#define DIALOGSPII2C_H

#include <QDialog>

namespace Ui
{
class Dialogspii2c;
}

class Dialogspii2c : public QDialog
{
    Q_OBJECT

public:
    explicit Dialogspii2c(QWidget *parent = nullptr);
    ~Dialogspii2c();

    QString spii2c_cfg_str;

signals:
    void send_string(QString);

private slots:
    void on_buttonbox_accepted();
    void rec_spii2c_string(QString str);
    void on_button_config_clicked();

private:
    Ui::Dialogspii2c *ui;
};

#endif  // DIALOGSPII2C_H
