#include "dialoguart.h"

#include "ui_dialoguart.h"

Dialoguart::Dialoguart(QWidget *parent) : QDialog(parent), ui(new Ui::Dialoguart)
{
    QStringList uart = {"B_HAL_UART_1", "B_HAL_UART_2", "B_HAL_UART_3",
                        "B_HAL_UART_4", "B_HAL_UART_5", "B_HAL_UART_6",
                        "B_HAL_UART_7", "B_HAL_UART_8", "B_HAL_LPUART_1"};
    ui->setupUi(this);
    ui->combox_uart->addItems(uart);

    uart_cfg_str.clear();

    this->setModal(true);
}

Dialoguart::~Dialoguart()
{
    delete ui;
}

void Dialoguart::on_buttonbox_accepted()
{
    uart_cfg_str = "{" + ui->combox_uart->currentText() + "}";
    emit send_string(uart_cfg_str);
}
