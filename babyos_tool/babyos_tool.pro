QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    ../config_wizard/cfg_wizard.cpp \
    ../dialog/dialoggpio.cpp \
    ../dialog/dialogi2c.cpp \
    ../dialog/dialoglcd.cpp \
    ../dialog/dialogqspi.cpp \
    ../dialog/dialogsdio.cpp \
    ../dialog/dialogspi.cpp \
    ../dialog/dialogspii2c.cpp \
    ../dialog/dialogspiqspi.cpp \
    ../dialog/dialogspisdio.cpp \
    ../dialog/dialoguart.cpp \
    main.cpp \
    mainwindow.cpp

HEADERS += \
    ../config_wizard/cfg_wizard.h \
    ../dialog/dialoggpio.h \
    ../dialog/dialogi2c.h \
    ../dialog/dialoglcd.h \
    ../dialog/dialogqspi.h \
    ../dialog/dialogsdio.h \
    ../dialog/dialogspi.h \
    ../dialog/dialogspii2c.h \
    ../dialog/dialogspiqspi.h \
    ../dialog/dialogspisdio.h \
    ../dialog/dialoguart.h \
    mainwindow.h

FORMS += \
    dialoggpio.ui \
    dialogi2c.ui \
    dialoglcd.ui \
    dialogqspi.ui \
    dialogsdio.ui \
    dialogspi.ui \
    dialogspii2c.ui \
    dialogspiqspi.ui \
    dialogspisdio.ui \
    dialoguart.ui \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RC_ICONS = icon.ico
