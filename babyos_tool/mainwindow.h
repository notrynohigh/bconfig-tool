#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QLabel>
#include <QMainWindow>
#include <QTreeWidget>

namespace Ui
{
class MainWindow;
}





typedef struct node_info
{
    QString                text;
    char                   type;
    int                    wtype;
    int                    line;
    unsigned int           min_val;
    unsigned int           max_val;
    QMap<QString, QString> cmap;
    QMap<QString, QString> map;
} OptInfoStruct_t;

typedef struct
{
    QString DrvName;
    QString IfName[2];
    int     MFlag;
    int     IfNum;
    int     SelectIndex;
} DrvCfgStruct_t;

#define LINE_NUM_MAX (1024)

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void btool_parse_cfgfile(QString dir, QTreeWidget *ptree);
    void btool_parse_bos(QString dir);
    void create_halif_dialog(QString drv_name);

private slots:
    void on_tree_config_itemClicked(QTreeWidgetItem *item, int column);
    void on_button_modify_sure_clicked();
    void on_button_config_halif_clicked();
    void rec_string(QString str);
    void on_button_clear_clicked();
    void on_button_save_config_clicked();
    void on_button_select_config_clicked();
    void on_button_select_bos_clicked();

private:
    Ui::MainWindow *ui;
    QLabel        *sbar_stat;
    QString       drv_halif_name;
    QString       drv_reg_name;
};

#endif  // MAINWINDOW_H
