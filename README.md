# BConfigTool

BabyOS的配置工具基本完成了，工具的代码写得比较粗糙，非常欢迎有大神能重写一份实现相同的功能。

做这个工具的愿意主要是两点：

1.让b_config.h方便配置，而不是局限于KEIL中。

2.让b_hal_if.h的代码写起来更方便，不用反复看当前接口对应的数据结构。

界面如下所示：

![](https://gitee.com/notrynohigh/bconfig-tool/raw/master/doc/1.png)






