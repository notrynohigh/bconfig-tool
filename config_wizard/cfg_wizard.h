#ifndef CFG_WIZARD_H
#define CFG_WIZARD_H

#include <QMainWindow>
#include <QTreeWidget>
#include <QVariant>
#include <QWidget>

typedef struct
{
    QString title;
} cfg_h_info_t;

typedef struct
{
    QString title;
    QString var_name;
    QString var_str;
} cfg_q_info_t;

#define O_PARAM_NONE (0)
#define O_PARAM_MAP (1)
#define O_PARAM_RANGE (2)

typedef struct
{
    QString                title;
    char                   param_type;
    QMap<QString, QString> param_map;
    int                    param_range[2];
    QString                var_name;
    QString                var_str;
} cfg_o_info_t;

typedef struct
{
    QString title;
    QString var_name;
    QString var_str;
} cfg_s_info_t;

typedef struct
{
    QString title;
    QString var_name;
    QString var_str;
} cfg_e_info_t;

typedef struct cfg_node
{
    char             cfg_type;
    cfg_h_info_t     h;
    cfg_q_info_t     q;
    cfg_o_info_t     o;
    cfg_s_info_t     s;
    cfg_e_info_t     e;
    QTreeWidgetItem *pitem;
    struct cfg_node *next;
    struct cfg_node *prev;
    struct cfg_node *child;
    struct cfg_node *parent;
} cfg_node_t;

class cfg_wizard
{
public:
    cfg_wizard();
    ~cfg_wizard();
    int     cfg_parse(QString file_name);
    QString cfg_node_title(cfg_node_t *pnode);

    cfg_node_t            cfg_node_head;
    QVector<cfg_node_t *> cfg_node_table;
    QString               cfg_file_data[3];
};

#endif  // CFG_WIZARD_H
